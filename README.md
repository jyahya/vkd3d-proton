# VKD3D

Camada de compatibilidade para o Direct3D 12 sob Vulkan, que permite executar aplicações gráficas feitas originalmente para Microsoft Windows no Linux.

Esta versão é baseada no [projeto original](https://github.com/HansKristian-Work/vkd3d-proton), mas mantendo sempre as últimas atualizações do git.

Este projeto foi criado para ser usado no *Proton-Async*, que é uma remasterização do [Proton.](https://github.com/ValveSoftware/Proton)

# Compilando e usando

Maiores informações podem ser encontradas no git do [projeto original](https://github.com/HansKristian-Work/vkd3d-proton)
